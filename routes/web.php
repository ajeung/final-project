<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Default
Route::get('/', function () {
    return view('default.welcome');
});

Route::get('/about', function () {
    return view('default.about');
});

Route::get('/destination', function () {
    return view('default.destination');
});

Route::get('/hotel', function () {
    return view('default.hotel');
});

Route::get('/register', function () {
    return view('default.register');
});

Route::get('/login', function () {
    return view('default.login');
});


// User
Route::get('/user', function () {
    return view('user.welcome');
});

Route::get('/user/about', function () {
    return view('user.about');
});

Route::get('/user/destination', function () {
    return view('user.destination');
});

Route::get('/user/hotel', function () {
    return view('user.hotel');
});

Route::get('/user/profile', function () {
    return view('user.profile');
});

Route::get('/user/edit', function () {
    return view('user.edit');
});

// Admin
Route::get('/admin', function () {
    return view('layouts.master-admin');
});

# User 
Route::get('/user', 'UserController@index');
Route::get('/user/create', 'UserController@create');
Route::post('/user', 'UserController@store');
Route::get('/user/{id}', 'UserController@show');
Route::get('/user/{user_id}/edit', 'UserController@edit');
Route::put('/user/{user_id}', 'UserController@update');
Route::delete('/user/{user_id}', 'UserController@destroy');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

# Hotel
Route::get('/admin/hotel', 'HotelController@index');
Route::get('/admin/hotel/create', 'HotelController@create');
Route::post('/admin/hotel', 'HotelController@store');
Route::get('/admin/hotel/{hotel_id}', 'HotelController@show');
Route::get('/admin/hotel/{hotel_id}/edit', 'HotelController@edit');
Route::put('/admin/hotel/{hotel_id}', 'HotelController@update');
Route::delete('/admin/hotel/{hotel_id}', 'HotelController@destroy');

# Destination
Route::get('/admin/destination', 'DestinationController@index');
Route::get('/admin/destination/create', 'DestinationController@create');
Route::post('/admin/destination', 'DestinationController@store');
Route::get('/admin/destination/{destination_id}', 'DestinationController@show');
Route::get('/admin/destination/{destination_id}/edit', 'DestinationController@edit');
Route::put('/admin/destination/{destination_id}', 'DestinationController@update');
Route::delete('/admin/destination/{destination_id}', 'DestinationController@destroy');