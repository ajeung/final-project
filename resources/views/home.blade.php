@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    {{-- You are logged in! --}}
                    @section('content')
                        <div class="container">
                            <table>
                                <tr>
                                    <td>Name</td>
                                    <td>:</td>
                                    {{-- <td>{{$users->nama}}</td> --}}
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td>:</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Mobile Phone &nbsp;&nbsp;</td>
                                    <td>:</td>
                                    <td></td>
                                </tr>
                            </table>
                        </div>
                    @endsection
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
