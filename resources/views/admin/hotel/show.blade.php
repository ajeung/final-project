@extends('layouts.master-admin')

@section('title')
    Show Hotel {{$hotel->id}}
@endsection

@section('content')
    <p>Hotel: {{$hotel->nama}}</p>
    <p>Total Room: {{$hotel->jumlah_kamar}}</p>
    <p>Location: {{$hotel->lokasi}}</p>
    <p>Price: {{$hotel->harga}}</p>
    <p>Description: {{$hotel->deskripsi}}</p>
@endsection

{{-- @extends('layouts.master')

@section('title')
    Show Destination {{$destination->id}}
@endsection

@section('content')
    <p>Destination: {{$destination->nama}}</p>
    <p>Location: {{$destination->lokasi}}</p>
    <p>Price: {{$destination->harga}}</p>
    <p>Rating: {{$destination->rating}}</p>
    <p>Description: {{$destination->deskripsi}}</p>
@endsection --}}