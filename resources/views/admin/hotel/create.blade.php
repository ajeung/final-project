@extends('layouts.master-admin')

@section('title')
    Add Hotel
@endsection

@section('content')
    <div>
        <h2>Add Hotel</h2>
        <form action="/admin/hotel" method="POST">
            @csrf
            <div class="form-group">
                <label for="nama">Hotel</label>
                <input type="text" class="form-control" name="nama" id="nama" placeholder="Hotel">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="jumlah_kamar">Total Room</label>
                <input type="text" class="form-control" name="jumlah_kamar" id="jumlah_kamar" placeholder="Total Room">
                @error('jumlah_kamar')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="lokasi">Location</label>
                <input type="text" class="form-control" name="lokasi" id="lokasi" placeholder="Location">
                @error('lokasi')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="harga">Price</label>
                <input type="text" class="form-control" name="harga" id="harga" placeholder="Price">
                @error('harga')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="deskripsi">Description</label>
                <input type="text" class="form-control" name="deskripsi" id="deskripsi" placeholder="Description">
                @error('deskripsi')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Add</button>
        </form>
    </div>
@endsection