@extends('layouts.master-admin')

@section('title')
    Hotels {{$hotel->id}}
@endsection

@section('content')
    <div>
        <h2>Edit Hotel {{$hotel->id}}</h2>
        <form action="/admin/hotel/{{$hotel->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="title">Name</label>
                <input type="text" class="form-control" name="nama" id="title" value="{{$hotel->nama}}" placeholder="Name">
                @error('nama')
                    <div class="alert alert-danger">
                        {{$message}}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">Room</label>
                <input type="text" class="form-control" name="jumlah_kamar" id="title" value="{{$hotel->jumlah_kamar}}" placeholder="Room">
                @error('umur')
                    <div class="alert alert-danger">
                        {{$message}}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">Location</label>
                <input type="text" class="form-control" name="lokasi" id="title" value="{{$hotel->lokasi}}" placeholder="Location">
                @error('bio')
                    <div class="alert alert-danger">
                        {{$message}}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">Price</label>
                <input type="text" class="form-control" name="harga" id="title" value="{{$hotel->harga}}" placeholder="Price">
                @error('harga')
                    <div class="alert alert-danger">
                        {{$message}}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">Description</label>
                <input type="text" class="form-control" name="deskripsi" id="title" value="{{$hotel->deskripsi}}" placeholder="description">
                @error('deskripsi')
                    <div class="alert alert-danger">
                        {{$message}}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Edit</button>
        </form>
</div>
@endsection