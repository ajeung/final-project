@extends('layouts.master-admin')

@section('title')
    Hotels
@endsection

@section('content')
    <a href="/admin/hotel/create" class="btn btn-primary">Tambah</a>
    <table class="table">
        <thead class="thead-light">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">Jumlah Kamar</th>
                <th scope="col">Lokasi</th>
                <th scope="col">Harga</th>
                <th scope="col">Deskripsi</th>
                <th scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($hotel as $key=>$value)
                <tr>
                    <td>{{$key + 1}}</th>
                    <td>{{$value->nama}}</td>
                    <td>{{$value->jumlah_kamar}}</td>
                    <td>{{$value->lokasi}}</td>
                    <td>{{$value->harga}}</td>
                    <td>{{$value->deskripsi}}</td>                   
                    <td>
                        <a href="/admin/hotel/{{$value->id}}" class="btn btn-info">Show</a>
                        <a href="/admin/hotel/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                        <form action="/admin/hotel/{{$value->id}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <input type="submit" class="btn btn-danger my-1" value="Delete">
                        </form>
                    </td>
                </tr>
            @empty
                <tr colspan="3">
                    <td>No data</td>
                </tr>  
            @endforelse              
        </tbody>
    </table>
@endsection