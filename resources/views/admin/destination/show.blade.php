@extends('layouts.master-admin')

@section('title')
    Show Destination {{$destination->id}}
@endsection

@section('content')
    <p>Destination: {{$destination->nama}}</p>
    <p>Location: {{$destination->lokasi}}</p>
    <p>Price: {{$destination->harga}}</p>
    <p>Rating: {{$destination->rating}}</p>
    <p>Description: {{$destination->deskripsi}}</p>
@endsection