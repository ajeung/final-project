@extends('layouts.master-admin')

@section('title')
    Add Destination
@endsection

@section('content')
    <div>
        <h2>Add Destination</h2>
        <form action="/admin/destination" method="POST">
            @csrf
            <div class="form-group">
                <label for="nama">Destination</label>
                <input type="text" class="form-control" name="nama" id="nama" placeholder="Destination">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="lokasi">Location</label>
                <input type="text" class="form-control" name="lokasi" id="lokasi" placeholder="Location">
                @error('lokasi')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="harga">Price</label>
                <input type="text" class="form-control" name="harga" id="harga" placeholder="Price">
                @error('harga')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="rating">Rating</label>
                <input type="text" class="form-control" name="rating" id="rating" placeholder="Rating">
                @error('rating')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="deskripsi">Description</label>
                <input type="text" class="form-control" name="deskripsi" id="deskripsi" placeholder="Description">
                @error('deskripsi')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Add</button>
        </form>
    </div>
@endsection