@extends('layouts.master-admin')

@section('title')
    Edit Destination
@endsection

@section('content')
    <div>
        <h2>Edit Destination {{$destination->id}}</h2>
        <form action="/admin/destination/{{$destination->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="nama">Destination</label>
                <input type="text" class="form-control" name="nama" id="nama" value="{{$destination->nama}}" placeholder="Destination">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="lokasi">Location</label>
                <input type="text" class="form-control" name="lokasi" id="lokasi" value="{{$destination->lokasi}}" placeholder="Location">
                @error('lokasi')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="harga">Price</label>
                <input type="text" class="form-control" name="harga" id="harga" value="{{$destination->harga}}" placeholder="Price">
                @error('harga')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="rating">Rating</label>
                <input type="text" class="form-control" name="rating" id="rating" value="{{$destination->rating}}" placeholder="Rating">
                @error('rating')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="deskripsi">Description</label>
                <input type="text" class="form-control" name="deskripsi" id="deskripsi" value="{{$destination->deskripsi}}" placeholder="Description">
                @error('deskripsi')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Save Changes</button>
        </form>
    </div>
@endsection