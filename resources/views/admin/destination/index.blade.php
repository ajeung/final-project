@extends('layouts.master-admin')

@section('title')
    Destinations
@endsection

@section('content')
<a href="/admin/destination/create" class="btn btn-primary">Add Destination</a>
    <table class="table">
        <thead class="thead-light">
            <tr>
                <th scope="col">No.</th>
                <th scope="col">Destination</th>
                <th scope="col">Location</th>
                <th scope="col">Price</th>
                <th scope="col">Rating</th>
                <th scope="col">Description</th>
                <th scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($destination as $key=>$value)
                <tr>
                    <td>{{$key + 1}}</th>
                    <td>{{$value->nama}}</td>
                    <td>{{$value->lokasi}}</td>
                    <td>{{$value->harga}}</td>
                    <td>{{$value->rating}}</td>
                    <td>{{$value->deskripsi}}</td>
                    <td>
                        <a href="/admin/destination/{{$value->id}}" class="btn btn-info">Show</a>
                        <a href="/admin/destination/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                        <form action="/admin/destination/{{$value->id}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <input type="submit" class="btn btn-danger my-1" value="Delete">
                        </form>
                    </td>
                </tr>
            @empty
                <tr colspan="3">
                    <td>No data</td>
                </tr>  
            @endforelse              
        </tbody>
    </table>
@endsection