<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <title>Register</title>
</head>
<body>
    <center>
        <br><br><br>
        <div class="d-inline-flex p-2 bd-highlight" style="text-align: left;">
            <span class="border border-dark rounded" style="width: 500px;">
                <br><br>
                <center>
                    <form>
                        <h2 style="color:black;">Login</h2><br>                        
                        <div class="form-row" style="width:400px;">
                            <div class="col">
                                <input type="text" name="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
                            </div>
                        </div>
                        <br>
                        <div class="form-row" style="width:400px;">
                            <div class="col">
                                <input type="password" name="password" class="form-control" id="exampleInputEmail1" placeholder="Password">
                            </div>
                        </div>
                        <br>                        
                        <center><button type="submit" class="btn btn-block btn-dark d-grid gap-2" style="width: 200px;">Register</button></center>
                        <br>
                        <center>Don't have an account? <a href="/register">Register here!</a></center>
                    </form>
                </center>
            <br>
            </span>
        </div>
    </center>
</body>
</html>
