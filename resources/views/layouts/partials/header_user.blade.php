<header id="header">
    <div class="container main-menu">
        <div class="row align-items-center justify-content-between d-flex">
          <div id="logo">
            <a href="/"><img src="img/logo.png" alt="" title="" /></a>
          </div>
          <nav id="nav-menu-container">
            <ul class="nav-menu">
              <li><a href="/user">Home</a></li>
              <li><a href="/user/about">About</a></li>
              <li><a href="/user/destination">Destination</a></li>
              <li><a href="/user/hotel">Hotels</a></li>				          					          		          
              <li><a href="/register">Profile</a></li>
            </ul>
          </nav>					      		  
        </div>
    </div>
</header>