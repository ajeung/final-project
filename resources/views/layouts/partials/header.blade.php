<header id="header">
    <div class="container main-menu">
        <div class="row align-items-center justify-content-between d-flex">
          <div id="logo">
            <a href="/"><img src="img/logo.png" alt="" title="" /></a>
          </div>
          <nav id="nav-menu-container">
            <ul class="nav-menu">
              <li><a href="/">Home</a></li>
              <li><a href="/about">About</a></li>
              <li><a href="/destination">Destination</a></li>
              <li><a href="/hotel">Hotels</a></li>				          					          		          
              <li><a href="/register">Register</a></li>
            </ul>
          </nav>					      		  
        </div>
    </div>
</header>