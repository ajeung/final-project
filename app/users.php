<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class users extends Model
{
    protected $table = "users";

    public function destination(){
        return $this->hasMany('App\destination');
    }

    public function booking(){
        return $this->hasMany('App\booking');
    }
}
