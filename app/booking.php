<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class booking extends Model
{
    protected $table = "booking";

    public function users(){
        return $this->belongsTo('App\booking');
    }

    public function hotel(){
        return $this->belongsTo('App\hotel');
    }

}
