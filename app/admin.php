<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class admin extends Model
{
    protected $table = "admin";

    public function destination(){
        return $this->hasMany('App\destination');
    }

    public function hotel(){
        return $this->hasMany('App\hotel');
    }
}
