<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class HotelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hotel = DB::table('hotel')->get();
        return view('admin.hotel.index', compact('hotel'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.hotel.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|unique:hotel',
            'jumlah_kamar' => 'required',
            'lokasi' => 'required',
            'harga' => 'required',
            'deskripsi' => 'required'
        ]);
        $query = DB::table('hotel')->insert([
            "nama" => $request["nama"],
            "jumlah_kamar" => $request["jumlah_kamar"],
            "lokasi" => $request["lokasi"],
            "harga" => $request["harga"],
            "deskripsi" => $request["deskripsi"]
        ]);
        return redirect('/admin/hotel');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $hotel = DB::table('hotel')->where('id', $id)->first();
        return view('admin.hotel.show', compact('hotel'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $hotel = DB::table('hotel')->where('id', $id)->first();
        return view('admin.hotel.edit', compact('hotel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'jumlah_kamar' => 'required',
            'lokasi' => 'required',
            'harga' => 'required',
            'deskripsi' => 'required'
        ]);
        $query = DB::table('hotel')->where('id', $id)->update([
            "nama" => $request["nama"],
            "jumlah_kamar" => $request["jumlah_kamar"],
            "lokasi" => $request["lokasi"],
            "harga" => $request["harga"],
            "deskripsi" => $request["deskripsi"]
        ]);
        return redirect('/admin/hotel');
    }
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $query = DB::table('hotel')->where('id', $id)->delete();
        return redirect('/admin/hotel');
    }
}
