<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class DestinationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $destination = DB::table('destination')->get();
        return view('admin.destination.index', compact('destination'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.destination.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'lokasi' => 'required',
            'harga' => 'required',
            'rating' => 'required',
            'deskripsi' => 'required'
        ]);
        $query = DB::table('destination')->insert([
            "nama" => $request["nama"],
            "lokasi" => $request["lokasi"],
            "harga" => $request["harga"],
            "rating" => $request["rating"],
            "deskripsi" => $request["deskripsi"]
        ]);
        return redirect('/admin/destination');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $destination = DB::table('destination')->where('id', $id)->first();
        return view('admin.destination.show', compact('destination'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $destination = DB::table('destination')->where('id', $id)->first();
        return view('admin.destination.edit', compact('destination'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update( $id, Request $request)
    {
        $request->validate([
            // 'id' => 'required',
            'nama' => 'required',
            'lokasi' => 'required',
            'harga' => 'required',
            'rating' => 'required',
            'deskripsi' => 'required'
        ]);
        $query = DB::table('destination')->where('id', $id)->update([
            // "id" => auth()->destination->id(),
            "nama" => $request["nama"],
            "lokasi" => $request["lokasi"],
            "harga" => $request["harga"],
            "rating" => $request["rating"],
            "deskripsi" => $request["deskripsi"]
        ]);
        return redirect('/admin/destination');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $query = DB::table('destination')->where('id', $id)->delete();
        return redirect('/admin/destination');
    }
}
