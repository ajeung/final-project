<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class UserController extends Controller
{
    public function create()
    {
        return view('user.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|unique:users',
            'email' => 'required',
            'password' => 'required',
            'telp' => 'required'
        ]);
        $query = DB::table('users')->insert([
            "nama" => $request["nama"],
            "email" => $request["email"],
            "password" => $request["password"],
            "telp" => $request["telp"]
        ]);
        return redirect('/user');
    }

    public function index()
    {
        $user = DB::table('users')->get();
        return view('user.welcome', compact('user'));
    }

    
    public function show($id)
    {
        $cast = DB::table('users')->where('id', $id)->first();
        return view('user.show', compact('user'));
    }

    
    public function edit($id)
    {
        $cast = DB::table('users')->where('id', $id)->first();
        return view('user.edit', compact('user'));
    }

    public function update($id, Request $request)
    {

        $request->validate([
            'nama' => 'required|unique:users',
            'email' => 'required',
            'password' => 'required',
            'telp' => 'required'
        ]);
        $query = DB::table('users')->where('id', $id)->update([
            "nama" => $request["nama"],
            "email" => $request["email"],
            "password" => $request["password"],
            "telp" => $request["telp"]
        ]);
        return redirect('/users');
    }

    
    public function destroy($id)
    {
        $query = DB::table('users')->where('id', $id)->delete();
        return redirect('/user');
    }
}
