<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class hotel extends Model
{
    protected $table = "hotel";

    public function booking(){
        return $this->belongsTo('App\booking');
    }

    public function admin(){
        return $this->belongsTo('App\admin');
    }
}
