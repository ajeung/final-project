<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class destination extends Model
{
    protected $table = "destination";

    public function users(){
        return $this->belongsTo('App\destination');
    }

    public function admin(){
        return $this->belongsTo('App\admin');
    }
}
 